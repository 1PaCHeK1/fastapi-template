import functools
from typing import ClassVar, TypeVar
from urllib.parse import quote_plus

from pydantic_settings import BaseSettings, SettingsConfigDict

TSettings = TypeVar("TSettings", bound=BaseSettings)


def get_settings(cls: type[TSettings]) -> TSettings:
    return cls()


get_settings = functools.lru_cache(get_settings)


class DatabaseSettings(BaseSettings):
    model_config = SettingsConfigDict(env_prefix="database_")

    driver: str = "postgresql+asyncpg"
    name: str
    username: str
    password: str
    host: str
    port: int = 5432
    echo: bool = False

    @property
    def url(self) -> str:
        password = quote_plus(self.password)
        return f"{self.driver}://{self.username}:{password}@{self.host}/{self.name}"


class SentrySettings(BaseSettings):
    model_config = SettingsConfigDict(env_prefix="sentry_")

    dsn: str = ""
    environment: str = "development"
    traces_sample_rate: float = 1.0


class AppSettings(BaseSettings):
    model_config = SettingsConfigDict(env_prefix="app_")

    allow_origins: ClassVar[list[str]] = []


class PasswordSettings(BaseSettings):
    model_config = SettingsConfigDict(env_prefix="password_")

    salt: bytes = b"2a"
    rounds: int = 10
