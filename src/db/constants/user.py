import enum


class UserRoleEnum(enum.Enum):
    client = "client"
    admin = "admin"
    manager = "manager"
