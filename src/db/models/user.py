from uuid import UUID

from sqlalchemy import Enum, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from db.base import Base, datetime_timezone, str_64, str_256, uuid_pk
from db.constants import UserRoleEnum


class User(Base):
    __tablename__ = "users"

    id: Mapped[uuid_pk]

    username: Mapped[str_64] = mapped_column(unique=True)
    email: Mapped[str_256]

    first_name: Mapped[str | None]
    surname: Mapped[str | None]

    password: Mapped[str_256]

    is_active: Mapped[bool] = mapped_column(insert_default=False)

    role: Mapped[UserRoleEnum] = mapped_column(
        Enum(UserRoleEnum, native_enum=False),
        insert_default=UserRoleEnum.client.value,
    )

    tokens: Mapped[list["UserToken"]] = relationship(
        back_populates="user",
        cascade="all",
    )


class UserToken(Base):
    __tablename__ = "userauthtokens"

    user_id: Mapped[UUID] = mapped_column(ForeignKey(User.id))
    platform: Mapped[str]
    client_app: Mapped[str]
    create_at: Mapped[datetime_timezone]
    token: Mapped[str_256] = mapped_column(primary_key=True)

    user: Mapped[User] = relationship(back_populates="tokens")
