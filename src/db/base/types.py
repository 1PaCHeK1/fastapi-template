import uuid
from datetime import datetime
from typing import Annotated

from sqlalchemy import (
    UUID,
    BigInteger,
    DateTime,
    Identity,
    Integer,
    String,
    Text,
)
from sqlalchemy.orm import mapped_column

uuid_pk = Annotated[
    uuid.UUID,
    mapped_column(
        UUID(as_uuid=True),
        primary_key=True,
        default=uuid.uuid4,
    ),
]

int32_pk = Annotated[
    int,
    mapped_column(
        Integer,
        primary_key=True,
        server_default=Identity(
            start=1,
            increment=1,
            minvalue=1,
            maxvalue=2147483647,
            cycle=False,
            cache=1,
        ),
    ),
]

int64_pk = Annotated[
    int,
    mapped_column(
        BigInteger,
        primary_key=True,
        server_default=Identity(
            start=1,
            increment=1,
            minvalue=1,
            maxvalue=9223372036854775807,
            cycle=False,
            cache=1,
        ),
    ),
]

int64 = Annotated[int, mapped_column(BigInteger)]

str_16 = Annotated[str, mapped_column(String(16))]
str_32 = Annotated[str, mapped_column(String(32))]
str_64 = Annotated[str, mapped_column(String(64))]
str_128 = Annotated[str, mapped_column(String(128))]
str_256 = Annotated[str, mapped_column(String(256))]

text = Annotated[str, mapped_column(Text)]

datetime_timezone = Annotated[datetime, mapped_column(DateTime(timezone=True))]
