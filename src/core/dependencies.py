from typing import Annotated

from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from db.dependencies import get_session

CoreAsyncSession = Annotated[AsyncSession, Depends(get_session)]
