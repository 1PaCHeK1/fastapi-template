from typing import TypeVar

from sqlalchemy import Select, func, select
from sqlalchemy.ext.asyncio import AsyncSession

from core.dto import PaginationDTO, PaginationResultDTO

_T = TypeVar("_T")


async def paginate(
    session: AsyncSession,
    stmt: Select[tuple[_T]],
    pagination: PaginationDTO,
) -> PaginationResultDTO[_T]:
    paginated = stmt.offset((pagination.page - 1) * pagination.page_size).limit(
        pagination.page_size,
    )
    count = await session.scalar(select(func.count()).select_from(stmt.subquery()))
    return PaginationResultDTO(
        items=(await session.execute(paginated)).scalars().unique().all(),
        count=count or 0,
    )
