import dataclasses
from collections.abc import Sequence
from typing import Generic, TypeVar

from .base import BaseDto

_T = TypeVar("_T")


class PaginationDTO(BaseDto):
    page: int
    page_size: int


@dataclasses.dataclass
class PaginationResultDTO(Generic[_T]):
    items: Sequence[_T]
    count: int
