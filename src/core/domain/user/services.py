import uuid

import bcrypt

from core.dependencies import CoreAsyncSession
from db.models import User
from settings import PasswordSettings, get_settings

from .dto import UserChangeDto, UserDto


class HashService:
    def __init__(self) -> None:
        self.settings = get_settings(PasswordSettings)

    def generate_password(self, raw_password: str) -> str:
        return bcrypt.hashpw(
            raw_password.encode(),
            bcrypt.gensalt(
                rounds=self.settings.rounds,
                prefix=self.settings.salt,
            ),
        ).decode()

    def check_password(self, raw_password: str, hashed_password: str) -> bool:
        return bcrypt.checkpw(raw_password.encode(), hashed_password.encode())

    def generate_token(self, user: User) -> str:  # noqa: ARG002
        return str(uuid.uuid4())


class UserService:
    def __init__(self, session: CoreAsyncSession) -> None:
        self._session = session

    async def get_user(self, uuid_key: uuid.UUID) -> UserDto | None:
        user_db = await self._session.get(User, uuid_key)
        if user_db is None:
            return None
        return UserDto.model_validate(user_db)

    async def change_user(self, user: User, user_changes: UserChangeDto) -> UserDto:
        user.username = user_changes.username
        user.first_name = user_changes.first_name
        user.surname = user_changes.surname

        self._session.add(user)
        await self._session.flush()
        return UserDto.model_validate(user)

    async def remove(self, user: User) -> None:
        await self._session.delete(user)
        await self._session.flush()
