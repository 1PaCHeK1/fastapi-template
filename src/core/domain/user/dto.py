import uuid

from core.dto import BaseDto
from db.constants import UserRoleEnum


class UserDto(BaseDto):
    id: uuid.UUID
    username: str
    email: str
    first_name: str | None
    surname: str | None
    is_active: bool
    role: UserRoleEnum


class UserChangeDto(BaseDto):
    username: str
    first_name: str | None
    surname: str | None
