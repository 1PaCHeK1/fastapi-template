from datetime import datetime, timezone
from typing import Annotated

from fastapi import Depends
from sqlalchemy import select

from core.dependencies import CoreAsyncSession
from core.domain.user import HashService
from db.constants import UserRoleEnum
from db.models import User, UserToken

from .dto import AuthFormDto, DeviceMeta, UserTokenDto
from .exceptions import AuthenticationError


class AuthService:
    def __init__(
        self,
        session: CoreAsyncSession,
        hash_service: Annotated[HashService, Depends()],
    ) -> None:
        self._session = session
        self._hash_service = hash_service

    async def __generate_token(self, user: User, device: DeviceMeta) -> UserToken:
        stmt = select(UserToken).where(
            UserToken.user_id == user.id,
            UserToken.platform == device.platform,
            UserToken.client_app == device.client_app,
        )
        token = await self._session.scalar(stmt)
        if token is not None:
            return token

        token = UserToken(
            user_id=user.id,
            platform=device.platform,
            client_app=device.client_app,
            create_at=datetime.now(tz=timezone.utc),
            token=self._hash_service.generate_token(user),
        )
        self._session.add(token)
        await self._session.flush()

        return token

    async def login(self, form: AuthFormDto) -> UserTokenDto:
        stmt = select(User).where(User.username == form.user.username)
        user = await self._session.scalar(stmt)
        if user is None or not self._hash_service.check_password(
            form.user.password,
            user.password,
        ):
            raise AuthenticationError

        return UserTokenDto.model_validate(await self.__generate_token(user, form.meta))

    async def login_admin(self, form: AuthFormDto) -> UserTokenDto:
        stmt = select(User).where(
            User.username == form.user.username,
            User.role.in_([UserRoleEnum.admin.value, UserRoleEnum.manager.value]),
        )
        user = await self._session.scalar(stmt)
        if user is None or not self._hash_service.check_password(
            form.user.password,
            user.password,
        ):
            raise AuthenticationError

        return UserTokenDto.model_validate(await self.__generate_token(user, form.meta))

    async def logout(self, user: User, token: str) -> None:
        stmt = select(UserToken).where(
            UserToken.user_id == user.id,
            UserToken.token == token,
        )
        instance = await self._session.scalar(stmt)
        if instance is not None:
            await self._session.delete(instance)
            await self._session.flush()

    async def get_active_token(self, user: User) -> list[UserTokenDto]:
        stmt = select(UserToken).where(UserToken.user_id == user.id)
        tokens = (await self._session.scalars(stmt)).all()
        return UserTokenDto.model_validate_list(tokens)
