from datetime import datetime

from core.dto import BaseDto


class DeviceMeta(BaseDto):
    client_app: str
    platform: str


class UserForm(BaseDto):
    username: str
    password: str


class AuthFormDto(BaseDto):
    user: UserForm
    meta: DeviceMeta


class UserTokenDto(BaseDto):
    platform: str
    client_app: str
    create_at: datetime
    token: str
