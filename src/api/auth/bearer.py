from dataclasses import dataclass
from typing import Annotated

from fastapi import Depends, Header, HTTPException
from fastapi.security import APIKeyHeader
from sqlalchemy import select
from sqlalchemy.orm import joinedload
from starlette import status
from starlette.requests import Request

from core.dependencies import CoreAsyncSession
from db.models import User, UserToken


class _BearerToken(APIKeyHeader):
    async def __call__(self, request: Request) -> str | None:
        api_key = await super().__call__(request)
        if not api_key:
            return None
        return api_key.removeprefix("Bearer ")


_auth_scheme = _BearerToken(name="Authorization", auto_error=False)

_unauthorized_exc = HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

AuthToken = Annotated[str | None, Depends(_auth_scheme)]


@dataclass
class DeviceInfo:
    platform: Annotated[
        str,
        Header(alias="sec-ch-ua-platform", include_in_schema=False),
    ]
    client_app: Annotated[str, Header(alias="sec-ch-ua", include_in_schema=False)]


DeviceInfoAnnotation = Annotated[DeviceInfo, Depends()]


@dataclass
class BearerAuth:
    platform: str
    client_app: str
    token: str
    user: User


async def authenticate(
    auth_token: AuthToken,
    session: CoreAsyncSession,
) -> BearerAuth:
    stmt = (
        select(UserToken)
        .join(UserToken.user)
        .options(joinedload(UserToken.user))
        .where(
            User.is_active,
            UserToken.token == auth_token,
        )
    )

    usertoken = await session.scalar(stmt)
    if usertoken is None:
        raise _unauthorized_exc

    return BearerAuth(
        platform=usertoken.platform,
        client_app=usertoken.client_app,
        token=usertoken.token,
        user=usertoken.user,
    )


Authenticate = Annotated[BearerAuth, Depends(authenticate)]


async def get_user(
    auth: Authenticate,
) -> User:
    return auth.user


CurrentUser = Annotated[User, Depends(get_user)]
