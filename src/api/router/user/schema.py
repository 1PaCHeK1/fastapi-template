import uuid

from api.schema import BaseSchema
from db.models.user import UserRoleEnum


class UserSchema(BaseSchema):
    id: uuid.UUID
    username: str
    email: str
    first_name: str | None
    surname: str | None
    is_active: bool
    role: UserRoleEnum


class UserActivationSchema(BaseSchema):
    token: str


class UserUpdateSchema(BaseSchema):
    username: str
    first_name: str
    surname: str


class UserResetPasswordSchema(BaseSchema):
    email: str


class UserResetPasswordConfirmSchema(BaseSchema):
    uuid: uuid.UUID
    token: str
    new_password: str


class RegisterationSchema(BaseSchema):
    username: str
    email: str
    firstname: str
    lastname: str
    password: str
