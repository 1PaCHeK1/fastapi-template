import uuid
from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException
from starlette import status

from api.auth import CurrentUser
from core.domain.user import UserService
from core.domain.user.dto import UserChangeDto

from .schema import (
    RegisterationSchema,
    UserActivationSchema,
    UserResetPasswordConfirmSchema,
    UserResetPasswordSchema,
    UserSchema,
    UserUpdateSchema,
)

router = APIRouter(
    tags=["user"],
    prefix="/user",
)

UserServiceAnnotation = Annotated[UserService, Depends()]


@router.post("/")
async def user_create(body: RegisterationSchema) -> UserSchema:
    raise NotImplementedError


@router.post("/activation")
async def user_activation(
    body: UserActivationSchema,
) -> UserSchema:
    raise NotImplementedError


@router.post("/reset-password")
async def user_reset_password(
    body: UserResetPasswordSchema,
) -> None:
    raise NotImplementedError


@router.post("/reset-password-confirm")
async def user_reset_password_confirm(
    body: UserResetPasswordConfirmSchema,
) -> None:
    raise NotImplementedError


@router.get("/me")
async def user_me(user: CurrentUser) -> UserSchema:
    return UserSchema.model_validate(user)


@router.get("/{uuid}")
async def user_by_uuid(
    user: CurrentUser,
    uuid: uuid.UUID,
    user_service: UserServiceAnnotation,
) -> UserSchema:
    user_dto = await user_service.get_user(uuid)
    if user_dto is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return UserSchema.model_validate(user_dto)


@router.put("/me")
async def user_change(
    user: CurrentUser,
    body: UserUpdateSchema,
    user_service: UserServiceAnnotation,
) -> UserSchema:
    return UserSchema.model_validate(
        await user_service.change_user(
            user,
            UserChangeDto.model_validate(body),
        ),
    )


@router.delete("/me", status_code=status.HTTP_204_NO_CONTENT)
async def user_delete(user: CurrentUser, user_service: UserServiceAnnotation) -> None:
    await user_service.remove(user)
