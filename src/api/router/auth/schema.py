from datetime import datetime

from api.schema import BaseSchema


class AuthSchema(BaseSchema):
    username: str
    password: str


class ResponseTokenSchema(BaseSchema):
    token: str


class ActiveTokenDeleteSchema(BaseSchema):
    token: str


class ActiveTokenSchema(BaseSchema):
    platform: str
    client_app: str
    create_at: datetime
