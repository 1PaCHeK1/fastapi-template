from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException
from starlette import status

from api.auth import Authenticate, CurrentUser, DeviceInfoAnnotation
from core.domain.auth import (
    AuthenticationError,
    AuthFormDto,
    AuthService,
    DeviceMeta,
    UserForm,
)

from .schema import (
    ActiveTokenDeleteSchema,
    ActiveTokenSchema,
    AuthSchema,
    ResponseTokenSchema,
)

router = APIRouter(
    tags=["auth"],
    prefix="/auth",
)

AuthServiceAnnotation = Annotated[AuthService, Depends()]


@router.post("/login")
async def login(
    device_info: DeviceInfoAnnotation,
    auth_service: AuthServiceAnnotation,
    body: AuthSchema,
) -> ResponseTokenSchema:
    if device_info.platform is None or device_info.client_app is None:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

    form = AuthFormDto(
        user=UserForm.model_validate(body),
        meta=DeviceMeta(
            platform=device_info.platform,
            client_app=device_info.client_app,
        ),
    )

    try:
        usertoken = await auth_service.login(form)
    except AuthenticationError as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST) from e
    else:
        token = usertoken.token
        return ResponseTokenSchema(token=token)


@router.get("/active-tokens")
async def active_tokens(
    user: CurrentUser,
    auth_service: AuthServiceAnnotation,
) -> list[ActiveTokenSchema]:
    tokens = await auth_service.get_active_token(user)
    return ActiveTokenSchema.model_validate_list(tokens)


@router.get("/logout", status_code=status.HTTP_204_NO_CONTENT)
async def logout(
    authenticate: Authenticate,
    auth_service: AuthServiceAnnotation,
) -> None:
    await auth_service.logout(authenticate.user, authenticate.token)


@router.post("/logout/device", status_code=status.HTTP_204_NO_CONTENT)
async def logout_device(
    user: CurrentUser,
    body: ActiveTokenDeleteSchema,
    auth_service: AuthServiceAnnotation,
) -> None:
    await auth_service.logout(user, body.token)
