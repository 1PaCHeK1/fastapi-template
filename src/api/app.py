import sentry_sdk
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from db.base.engine import engine
from settings import AppSettings, SentrySettings, get_settings

from .middleware import CommitSessionMiddleware
from .router import router


def sentry_init() -> None:
    settings = get_settings(SentrySettings)
    sentry_sdk.init(
        dsn=settings.dsn,
        environment=settings.environment,
        traces_sample_rate=settings.traces_sample_rate,
    )


def create_app() -> FastAPI:
    sentry_init()
    app_settings = get_settings(AppSettings)
    app = FastAPI()
    app.add_middleware(CommitSessionMiddleware)
    app.add_middleware(
        CORSMiddleware,
        allow_origins=app_settings.allow_origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    app.include_router(router)

    @app.get("/health")
    async def healthcheck() -> None:
        return None

    @app.on_event("shutdown")
    async def on_shutdown() -> None:
        await engine.dispose()  # pragma: no coverage

    return app
