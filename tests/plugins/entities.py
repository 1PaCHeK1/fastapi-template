import uuid
from datetime import datetime, timezone

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from core.domain.user import HashService
from core.domain.user.dto import UserDto
from db.constants import UserRoleEnum
from db.models import User
from db.models.user import UserToken


@pytest.fixture
def password() -> str:
    return "password"


@pytest.fixture
def platform() -> str:
    return "Window"


@pytest.fixture
def device() -> str:
    return "Google Chrome"


@pytest.fixture
def client_dto() -> UserDto:
    return UserDto(
        id=uuid.uuid4(),
        username="Test user",
        email="client@email.com",
        first_name="FirstName",
        surname="Surname",
        is_active=True,
        role=UserRoleEnum.client,
    )


@pytest.fixture
def admin_dto() -> UserDto:
    return UserDto(
        id=uuid.uuid4(),
        username="Test admin",
        email="admin@email.com",
        first_name="FirstAdmin",
        surname="Admin",
        is_active=True,
        role=UserRoleEnum.admin,
    )


@pytest.fixture
async def client(
    session: AsyncSession,
    client_dto: UserDto,
    password: str,
    hash_service: HashService,
) -> User:
    user = User(
        id=client_dto.id,
        password=hash_service.generate_password(password),
        username=client_dto.username,
        email=client_dto.email,
        first_name=client_dto.first_name,
        surname=client_dto.surname,
        role=client_dto.role,
        is_active=client_dto.is_active,
    )
    session.add(user)
    await session.flush()
    return user


@pytest.fixture
async def admin(
    session: AsyncSession,
    admin_dto: UserDto,
    password: str,
    hash_service: HashService,
) -> User:
    user = User(
        id=admin_dto.id,
        password=hash_service.generate_password(password),
        username=admin_dto.username,
        email=admin_dto.email,
        first_name=admin_dto.first_name,
        surname=admin_dto.surname,
        role=admin_dto.role,
        is_active=admin_dto.is_active,
    )
    session.add(user)
    await session.flush()
    return user


@pytest.fixture
async def client_token(
    session: AsyncSession,
    client: User,
    platform: str,
    device: str,
) -> UserToken:
    token = UserToken(
        user_id=client.id,
        platform=platform,
        client_app=device,
        create_at=datetime.now(tz=timezone.utc),
        token=uuid.uuid4().hex,
    )
    session.add(token)
    await session.flush()
    return token


@pytest.fixture
async def admin_token(
    session: AsyncSession,
    admin: User,
    platform: str,
    device: str,
) -> UserToken:
    token = UserToken(
        user_id=admin.id,
        platform=platform,
        client_app=device,
        create_at=datetime.now(tz=timezone.utc),
        token=uuid.uuid4().hex,
    )
    session.add(token)
    await session.flush()
    return token
