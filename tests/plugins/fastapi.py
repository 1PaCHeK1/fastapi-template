from collections.abc import AsyncIterator

import httpx
import pytest
from fastapi import FastAPI

from db.models import UserToken


@pytest.fixture
def fastapi_app() -> FastAPI:
    from api.app import create_app

    return create_app()


@pytest.fixture
async def http_client(fastapi_app: FastAPI) -> AsyncIterator[httpx.AsyncClient]:
    async with httpx.AsyncClient(
        app=fastapi_app,
        base_url="http://test",
        headers={
            "sec-ch-ua-platform": "test-platfrom",
            "sec-ch-ua": "test-device",
        },
    ) as client:
        yield client


@pytest.fixture
async def authenticated_http_client(
    fastapi_app: FastAPI,
    client_token: UserToken,
) -> AsyncIterator[httpx.AsyncClient]:
    async with httpx.AsyncClient(
        app=fastapi_app,
        base_url="http://test",
        headers={
            "Authorization": f"Bearer {client_token.token}",
            "sec-ch-ua-platform": "test-platfrom",
            "sec-ch-ua": "test-device",
        },
    ) as client:
        yield client


@pytest.fixture
async def admin_http_client(
    fastapi_app: FastAPI,
    admin_token: UserToken,
) -> AsyncIterator[httpx.AsyncClient]:
    async with httpx.AsyncClient(
        app=fastapi_app,
        base_url="http://test",
        headers={
            "Authorization": f"Bearer {admin_token.token}",
            "sec-ch-ua-platform": "test-platfrom",
            "sec-ch-ua": "test-device",
        },
    ) as client:
        yield client
