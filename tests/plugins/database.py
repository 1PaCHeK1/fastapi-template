import asyncio
import os
import pathlib
from collections.abc import AsyncIterator, Iterable

import pytest
import sqlalchemy as sa
from sqlalchemy.engine import Connection
from sqlalchemy.ext.asyncio import (
    AsyncEngine,
    AsyncSession,
    async_sessionmaker,
    create_async_engine,
)

import db.base
from alembic import command, config

base_dir = pathlib.Path(__file__).resolve().parent.parent


@pytest.fixture(scope="session")
def anyio_backend() -> str:
    return "asyncio"


@pytest.fixture(scope="session")
def event_loop() -> Iterable[asyncio.AbstractEventLoop]:
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def database_url() -> str:
    return os.environ["DATABASE_TEST_URL"]


@pytest.fixture(scope="session")
def sqlalchemy_pytest_database_url(database_url: str, worker_id: str) -> str:
    return f"{database_url}-{worker_id}"


@pytest.fixture(scope="session")
def alembic_config() -> config.Config:
    return config.Config("alembic.ini")


@pytest.fixture(scope="session")
def sqlalchemy_pytest_engine(sqlalchemy_pytest_database_url: str) -> AsyncEngine:
    return create_async_engine(sqlalchemy_pytest_database_url)


@pytest.fixture(scope="session")
def sessionmaker() -> async_sessionmaker[AsyncSession]:
    return db.base.async_session_factory


@pytest.fixture(scope="session")
async def _create_database(
    sqlalchemy_pytest_database_url: str,
) -> AsyncIterator[None]:
    database_url, database_name = sqlalchemy_pytest_database_url.rsplit("/", 1)
    engine = create_async_engine(
        database_url,
        execution_options={"isolation_level": "AUTOCOMMIT"},
    )
    async with engine.connect() as conn:
        exists = await conn.scalar(
            sa.text(
                f"SELECT 1 FROM pg_database WHERE datname='{database_name}'",  # noqa: S608
            ),
        )
        if not exists:
            await conn.execute(sa.text(f'create database "{database_name}";'))

    yield

    async with engine.connect() as conn:
        await conn.execute(
            sa.text(
                f"""
                select pg_terminate_backend(pg_stat_activity.pid)
                from pg_stat_activity
                where pg_stat_activity.datname = '{database_name}'
                and pid <> pg_backend_pid();
                """,  # noqa: S608
            ),
        )
        await conn.execute(sa.text(f'drop database "{database_name}";'))


@pytest.fixture(scope="session")
async def _run_migrations(
    _create_database: None,
    sqlalchemy_pytest_engine: AsyncEngine,
    alembic_config: config.Config,
    database_url: str,
) -> None:
    def run_upgrade(connection: Connection, cfg: config.Config) -> None:
        cfg.attributes["connection"] = connection
        command.upgrade(cfg, revision="head")

    async with sqlalchemy_pytest_engine.begin() as conn:
        alembic_config.set_main_option("sqlalchemy.url", database_url)
        await conn.run_sync(run_upgrade, alembic_config)
        await conn.commit()


@pytest.fixture(autouse=True)
async def session(
    _run_migrations: None,
    sqlalchemy_pytest_engine: AsyncEngine,
    sessionmaker: async_sessionmaker[AsyncSession],
) -> AsyncIterator[AsyncSession]:
    async with sqlalchemy_pytest_engine.connect() as conn:
        transaction = await conn.begin()
        sessionmaker.configure(bind=conn)

        async with sessionmaker() as session:
            yield session

        if transaction.is_active:
            await transaction.rollback()
