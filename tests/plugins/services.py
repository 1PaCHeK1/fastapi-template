import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from core.domain.auth.services import AuthService
from core.domain.user.services import HashService, UserService


@pytest.fixture
def hash_service() -> HashService:
    return HashService()


@pytest.fixture
def user_service(session: AsyncSession) -> UserService:
    return UserService(session=session)


@pytest.fixture
def auth_service(
    hash_service: HashService,
    session: AsyncSession,
) -> AuthService:
    return AuthService(
        session=session,
        hash_service=hash_service,
    )
