import dotenv

dotenv.load_dotenv(".env")
pytest_plugins = [
    "anyio",
    "tests.plugins.database",
    "tests.plugins.entities",
    "tests.plugins.services",
    "tests.plugins.fastapi",
    "tests.plugins.fixture_typecheck",
]
