from datetime import datetime

import httpx
import pytest
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from db.models import User, UserToken

pytestmark = [pytest.mark.anyio]


@pytest.fixture
def login_body(client: User) -> dict[str, str]:
    return {
        "username": client.username,
        "password": "password",
    }


async def test_login(
    http_client: httpx.AsyncClient,
    login_body: dict[str, str],
) -> None:
    response = await http_client.post(
        "/auth/login",
        json=login_body,
    )
    assert response.status_code == status.HTTP_200_OK


async def test_relogin(
    authenticated_http_client: httpx.AsyncClient,
    login_body: dict[str, str],
) -> None:
    response = await authenticated_http_client.post(
        "/auth/login",
        json=login_body,
    )
    assert response.status_code == status.HTTP_200_OK


async def test_login_invalid(
    authenticated_http_client: httpx.AsyncClient,
) -> None:
    response = await authenticated_http_client.post(
        "/auth/login",
        json={
            "username": "invalid username",
            "password": "invalid password",
        },
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST


async def test_logout(
    authenticated_http_client: httpx.AsyncClient,
) -> None:
    response = await authenticated_http_client.get("auth/logout")
    assert response.status_code == status.HTTP_204_NO_CONTENT

    response = await authenticated_http_client.get("user/me")
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


async def test_get_tokens(
    session: AsyncSession,
    authenticated_http_client: httpx.AsyncClient,
    client: User,
) -> None:
    response = await authenticated_http_client.get("auth/active-tokens")
    assert response.status_code == status.HTTP_200_OK
    data = [
        datetime.fromisoformat(item["createAt"]).isoformat() for item in response.json()
    ]
    stmt = select(UserToken.create_at).where(UserToken.user_id == client.id)
    tokens = (await session.scalars(stmt)).all()

    assert sorted(data) == sorted(map(datetime.isoformat, tokens))


async def test_remove_token(
    session: AsyncSession,
    authenticated_http_client: httpx.AsyncClient,
    client_token: UserToken,
) -> None:
    response = await authenticated_http_client.post(
        "auth/logout/device",
        json={
            "token": client_token.token,
        },
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT

    stmt = select(UserToken).where(UserToken.user_id == client_token.user_id)

    tokens = (await session.scalars(stmt)).all()
    assert tokens == []


async def test_duplicate_token(  # noqa: PLR0913
    session: AsyncSession,
    authenticated_http_client: httpx.AsyncClient,
    client: User,
    client_token: UserToken,
    login_body: dict[str, str],
    platform: str,
    device: str,
) -> None:
    response = await authenticated_http_client.post(
        "/auth/login",
        json=login_body,
        headers={
            "sec-ch-ua-platform": platform,
            "sec-ch-ua": device,
        },
    )

    assert response.status_code == status.HTTP_200_OK
    stmt = select(UserToken).where(UserToken.user_id == client.id)

    tokens = (await session.scalars(stmt)).all()
    assert tokens == [client_token]
