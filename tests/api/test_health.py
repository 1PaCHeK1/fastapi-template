import httpx
import pytest
from starlette import status

pytestmark = [pytest.mark.anyio]


async def test_login(http_client: httpx.AsyncClient) -> None:
    response = await http_client.get("/health")
    assert response.status_code == status.HTTP_200_OK
