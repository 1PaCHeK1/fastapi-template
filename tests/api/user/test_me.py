import httpx
import pytest
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from api.router.user.schema import UserUpdateSchema
from db.models import User

pytestmark = [pytest.mark.anyio]


async def test_get_me(
    authenticated_http_client: httpx.AsyncClient,
    client: User,
) -> None:
    response = await authenticated_http_client.get("/user/me")
    assert response.status_code == status.HTTP_200_OK
    data = response.json()

    assert data == {
        "id": str(client.id),
        "username": client.username,
        "email": client.email,
        "firstName": client.first_name,
        "surname": client.surname,
        "isActive": client.is_active,
        "role": client.role.value,
    }


async def test_get_me_no_auth(
    http_client: httpx.AsyncClient,
) -> None:
    response = await http_client.get("/user/me")
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


async def test_change_me(
    session: AsyncSession,
    authenticated_http_client: httpx.AsyncClient,
    client: User,
) -> None:
    new_first_name = "new_first_name"
    client.first_name = new_first_name
    json_data = UserUpdateSchema.model_validate(client).model_dump()
    response = await authenticated_http_client.put(
        "/user/me",
        json=json_data,
    )
    assert response.status_code == status.HTTP_200_OK
    await session.refresh(client)

    assert client.first_name == new_first_name


async def test_delete_me(
    session: AsyncSession,
    authenticated_http_client: httpx.AsyncClient,
    client: User,
) -> None:
    response = await authenticated_http_client.delete("/user/me")
    assert response.status_code == status.HTTP_204_NO_CONTENT

    user = await session.scalar(select(User).where(User.id == client.id))
    assert user is None
