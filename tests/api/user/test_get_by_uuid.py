import uuid

import httpx
import pytest
from fastapi import FastAPI
from starlette import status

from db.models import User

pytestmark = [pytest.mark.anyio]


async def test_get_by_uuid(
    authenticated_http_client: httpx.AsyncClient,
    fastapi_app: FastAPI,
    client: User,
) -> None:
    response = await authenticated_http_client.get(
        fastapi_app.url_path_for(
            "user_by_uuid",
            uuid=client.id,
        ),
    )
    assert response.status_code == status.HTTP_200_OK
    data = response.json()

    assert data == {
        "id": str(client.id),
        "username": client.username,
        "email": client.email,
        "firstName": client.first_name,
        "surname": client.surname,
        "isActive": client.is_active,
        "role": client.role.value,
    }


async def test_get_by_uuid_not_found(
    authenticated_http_client: httpx.AsyncClient,
    fastapi_app: FastAPI,
) -> None:
    response = await authenticated_http_client.get(
        fastapi_app.url_path_for(
            "user_by_uuid",
            uuid=str(uuid.uuid4()),
        ),
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND
