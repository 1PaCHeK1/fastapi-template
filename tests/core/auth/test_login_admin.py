import pytest

from core.domain.auth import AuthFormDto, AuthService, DeviceMeta, UserForm
from core.domain.auth.exceptions import AuthenticationError
from db.models import User

pytestmark = [pytest.mark.anyio]


async def test_login_client(
    client: User,
    password: str,
    auth_service: AuthService,
) -> None:
    form = AuthFormDto(
        user=UserForm(
            username=client.username,
            password=password,
        ),
        meta=DeviceMeta(
            client_app="test",
            platform="test",
        ),
    )
    with pytest.raises(AuthenticationError):
        await auth_service.login_admin(form)


async def test_login_admin(
    admin: User,
    password: str,
    auth_service: AuthService,
) -> None:
    form = AuthFormDto(
        user=UserForm(
            username=admin.username,
            password=password,
        ),
        meta=DeviceMeta(
            client_app="test",
            platform="test",
        ),
    )

    assert await auth_service.login_admin(form)
